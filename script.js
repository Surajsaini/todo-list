$('button').click(function() {
    let a = $('#aa').val();
    let b = $('#bb').val();

    let tasks = [];

    if (localStorage.getItem("tasks")) {
        tasks = JSON.parse(localStorage.getItem("tasks"));
        tasks.push({ "category": a, "taskname": b });
        localStorage.setItem("tasks", JSON.stringify(tasks));
    } else {
        tasks[0] = { "category": a, "taskname": b };
        localStorage.setItem("tasks", JSON.stringify(tasks));
    }
    showTasks();
});

function showTasks() {
    let alltasks = [];

    if (localStorage.getItem("tasks")) {
        alltasks = JSON.parse(localStorage.getItem("tasks"));
        $("#tasks").empty();

        alltasks.forEach((element, index) => {
            $("#tasks").append('<tr><td class="line1"><b>' + element.category + '</b> : ' + element.taskname + '</td><td class="text-right"><button class="delete" onClick="deleteTask(' + index + ')">Del</button>' + '</td></tr>');
        });
    }
}

function deleteTask(index) {
    if (localStorage.getItem("tasks")) {
        let tasks = JSON.parse(localStorage.getItem("tasks"));

        tasks.splice(index, 1);

        localStorage.setItem("tasks", JSON.stringify(tasks));
        showTasks();
    }
}

showTasks();